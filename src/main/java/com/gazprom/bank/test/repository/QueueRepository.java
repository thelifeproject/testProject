package com.gazprom.bank.test.repository;

import com.gazprom.bank.test.model.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QueueRepository extends JpaRepository<ItemEntity, Integer> {
}
