package com.gazprom.bank.test.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GettingItemsResponseDto implements Serializable {
    private Integer dequeuedItems = 0;
    private Integer cachedItems = 0;
    private Boolean result = false;
}
