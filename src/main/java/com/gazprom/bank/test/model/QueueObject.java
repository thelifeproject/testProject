package com.gazprom.bank.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class QueueObject implements Serializable {
    private Integer ownerId;
    private String message;
}
