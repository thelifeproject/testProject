package com.gazprom.bank.test.controller;

import com.gazprom.bank.test.model.dto.ErrorDto;
import com.gazprom.bank.test.service.QueueService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/api/v1/queue")
public class QueueController {
    private final QueueService queueService;

    private final static String VALUE_ONE = "one";
    private final static String VALUE_TWO = "two";

    @GetMapping("/{value}")
    public @ResponseBody ResponseEntity save(@PathVariable String value) {
        try {
            switch (value) {
                case (VALUE_ONE):
                    return new ResponseEntity<>(queueService.getObjectsFromQueue(), HttpStatus.OK);
                case (VALUE_TWO):
                    return new ResponseEntity<>(queueService.saveObjectsFromCache(), HttpStatus.OK);
                default:
                    return new ResponseEntity<>(new ErrorDto("Bad Request", 400), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(new ErrorDto(ex.getMessage(), 500), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
