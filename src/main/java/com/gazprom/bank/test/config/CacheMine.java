package com.gazprom.bank.test.config;


import com.gazprom.bank.test.model.QueueObject;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Getter
@Setter
public class CacheMine {

    private static final CacheMine INSTANCE = new CacheMine();

    private CacheMine() {

    }
    public static CacheMine getInstance() {
        return INSTANCE;
    }
    private List<QueueObject> cache = new ArrayList<>();
}
