package com.gazprom.bank.test;

import com.gazprom.bank.test.model.QueueObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import javax.jms.*;

@SpringBootApplication
@Slf4j
public class TestProjectApplication {

    public static void main(String[] args) {

        SpringApplication.run(TestProjectApplication.class, args);
        //fillingQueueBatch();
    }

    /**
     * Наполнитель очереди для тестирования.
     */
    public static void fillingQueueBatch() {
        for (int i = 1; i < 8; i++) {
            var item = new QueueObject(i, "Message number " + i);
            fillingQueue(item);
        }
    }

    public static void fillingQueue(QueueObject item) {
        try {
            var connectionFactory = new ActiveMQConnectionFactory("vm://localhost");

            var connection = connectionFactory.createConnection();
            connection.start();

            var session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            var destination = session.createQueue("test.queue");

            var producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            ObjectMessage message = session.createObjectMessage(item);
            producer.send(message);
            session.close();
            connection.close();
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

}
