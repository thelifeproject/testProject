package com.gazprom.bank.test.service;

import com.gazprom.bank.test.model.dto.GettingItemsResponseDto;
import com.gazprom.bank.test.model.entity.ItemEntity;

import java.util.List;

public interface QueueService {
    GettingItemsResponseDto getObjectsFromQueue();
    List<ItemEntity> saveObjectsFromCache();
}
