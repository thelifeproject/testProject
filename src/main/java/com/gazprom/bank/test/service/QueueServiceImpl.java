package com.gazprom.bank.test.service;

import com.gazprom.bank.test.config.CacheMine;
import com.gazprom.bank.test.model.QueueObject;
import com.gazprom.bank.test.model.dto.GettingItemsResponseDto;
import com.gazprom.bank.test.model.entity.ItemEntity;
import com.gazprom.bank.test.repository.QueueRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.jms.*;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class QueueServiceImpl implements QueueService{

    private final CacheMine cache = CacheMine.getInstance();

    @Value("${spring.activemq.url}")
    private String queueUrl;

    @Value("${spring.activemq.name}")
    private String queueName;

    @Autowired
    private QueueRepository repository;

    /**
     * Получение объектов из очереди и сохранение в кэш
     *
     * @return результат работы получения объектов из очереди и складывания в кэш
     */
    @Override
    public GettingItemsResponseDto getObjectsFromQueue() {
        var response = new GettingItemsResponseDto();
        for (int i = 0; i < 5; i++) {
            var item = getItemFromQueue();
            if (item != null) {
                response.setDequeuedItems(response.getDequeuedItems() + 1);
                cache.getCache().add(item);
                response.setCachedItems(response.getCachedItems() + 1);
            }
        }
        if (response.getDequeuedItems().equals(response.getCachedItems()) && response.getDequeuedItems() == 5) {
            response.setResult(true);
        }
        return response;
    }


    /**
     * Получение объектов из кэша для сохранения в БД
     */
    @Override
    public List<ItemEntity> saveObjectsFromCache() {
        List<QueueObject> list;
        var flag = cache.getCache().size() > 5;
        if (flag) {
            list = cache.getCache().subList(0, 4);
        } else {
            list = cache.getCache();
        }
        List<ItemEntity> toSave = new ArrayList<>();
        list.forEach(queue -> {
            var entity = new ItemEntity();
            entity.setOwnerId(queue.getOwnerId());
            entity.setMessage(queue.getMessage());
            toSave.add(entity);
        });
        var result = repository.saveAll(toSave);
        if (flag) {
            cache.setCache(cache.getCache().subList(5, cache.getCache().size() - 1));
        } else {
            cache.setCache(new ArrayList<>());
        }
        return result;
    }

    /**
     * Вычитка сообщений из очереди по одному
     *
     * @return Объект очереди
     */
    private QueueObject getItemFromQueue() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(queueUrl);

            Connection connection = connectionFactory.createConnection();
            connection.start();


            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            Destination destination = session.createQueue(queueName);

            MessageConsumer consumer = session.createConsumer(destination);

            Message message = consumer.receive();
            QueueObject item = null;
            if (message instanceof ObjectMessage) {
                ObjectMessage objectMessage = (ObjectMessage) message;
                item = (QueueObject) objectMessage.getObject();
                if (item != null) {
                    log.info("Received queueObject with ownerId " + item.getOwnerId());
                } else {
                    log.info("Received not queueObject");
                }
            } else {
                log.info("Received not ObjectMessage");
            }

            consumer.close();
            session.close();
            connection.close();
            return item;
        } catch (Exception e) {
            log.error("Something gone wrong " + e.getMessage());
            return null;
        }
    }
}
